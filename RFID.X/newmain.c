/*
 * File:   newmain.c
 * Author: moyukh
 *
 * Created on December 31, 2018, 4:04 PM
 */


#include <xc.h>
#include <pic18f4550.h>
#include "pic18f4550_config.h"
#include "uart.h"
#include "spi.h"
#define _XTAL_FREQ 8000000

int i = 1;

void gsm_init(void) {
    TRISD = 0x00; //PORTB as Output
    UART_Init(115200);
    PORTD = 0xFF;
    __delay_ms(20000);
    UART_Write_Text("ATE1\r\n");
    __delay_ms(10);
    UART_Write_Text("AT+CMGF=1\r\n");
    __delay_ms(300);
    UART_Write_Text("AT+CGATT=1\r\n");
    __delay_ms(300);
    UART_Write_Text("AT+CIPMUX=0\r\n");
    __delay_ms(300);
    UART_Write_Text("AT+CSTT= \"internet\"\r\n");
    __delay_ms(20000);
}

void send_data(char *text){
    TRISB = 0x00; //PORTB as Output
    UART_Init(115200);
    PORTB = 0b00010000;
    UART_Write_Text("AT\r\n");
    __delay_ms(10);
    UART_Write_Text("AT+CSTT?\r\n");
    __delay_ms(20000);
    UART_Write_Text("AT+CIICR\r\n");
    __delay_ms(20000);
    UART_Write_Text("AT+CIFSR\r\n");
    __delay_ms(300);
    UART_Write_Text("AT+CIPSTART=\"TCP\",\"dev.finder-lbs.com\",\"9998\"\r\n");
    __delay_ms(20000);
    UART_Write_Text("AT+CIPSTATUS\r\n");
    __delay_ms(20000);
    UART_Write_Text("AT+CIPSEND\r\n");
    __delay_ms(20000);
    UART_Write_Text(text);
    __delay_ms(300);
    UART_Write('\x1A');
    __delay_ms(100);
    UART_Write('\r');
    __delay_ms(100);
    UART_Write_Text("AT+CIPCLOSE\r\n");
    __delay_ms(300);
}

void main(void) {
    gsm_init();
    while (1) {
        PORTB = 0b00010000;
        if (i<10){
            send_data("AT KAJ Korse");
            PORTD = 0b00000000;
            __delay_ms(10000);
            PORTD = 0b11111111;
            i++;
        }
    }
}
